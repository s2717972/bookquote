package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {


    //isbn 1 2 3 4 5 others
    //result 10.0 45.0 20.0 35.0 50.0 0.0
    public double getBookPrice(String isbn){
        HashMap<String, Double> result = new HashMap<>();
        result.put("1", 10.0);
        result.put("2", 45.0);
        result.put("3", 20.0);
        result.put("4", 35.0);
        result.put("5", 50.0);
        result.put("others", 0.0);
        return result.get(isbn);
    }
    public double convert(String isbn){

        return Double.parseDouble(isbn)/2 + 30;
    }
}
